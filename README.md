# Pour compiler l'image
docker build -t serveur-image .
# Pour lancer l'image avec un volume
docker run -d -p 3000:3000 -v $(pwd)/images:/images serveur-image

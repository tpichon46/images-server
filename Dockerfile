FROM node:latest
WORKDIR /
RUN mkdir images
COPY package*.json .
RUN npm install 
COPY app.js .
EXPOSE 3000
CMD ["node", "app.js"]

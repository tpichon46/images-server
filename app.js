const express = require('express');
const multer = require('multer');
const sharp = require('sharp');
const fs = require('fs');

const app = express();
const port = 3000;
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

// Quand le serveur reçoit une requête POST contenant une image, il effectue une conversion de l'image au format WebP
// et enregistre l'image dans le dossier images avec un numéro aléatoire.
app.post("/", upload.single("image"), async (req, res) => {
  if (!req.file) {
    return res.status(400).send("Erreur 400 (Bad Request), image manquante ou format non accepté ?");
  }

  try {
    // Generation d'un id
    const image_id = crypto.randomUUID();

    // Conversion de l'image en WebP
    const buffer = await sharp(req.file.buffer)
      .webp()
      .toBuffer();

    // Enregistrement de l'image
    await sharp(buffer).toFile(`images/${image_id}.webp`);

    // Retourne le numéro de l'image
    res.send({ id: image_id });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

app.post("/:id", upload.single("image"), async (req, res) => {
    const image_id = req.params.id;
    const path = `images/${image_id}.webp`;


    if (req.file) {
        try {
            // Conversion de l'image en WebP
            const buffer = await sharp(req.file.buffer)
                .webp()
                .toBuffer();

			if (fs.existsSync(path)) {
				// Supprime l'ancienne image
				await fs.promises.unlink(path);
			}
            // Enregistrement de l'image
            await sharp(buffer).toFile(`images/${image_id}.webp`);

            // Retourne le numéro de l'image
            res.send({ id : image_id });


	} catch (error) {
		res.status(500).send(error.message);
	}
    } else {
        return res.status(400).send("Erreur 400 (Bad Request), id/image manquante ou format non accepté ?");
    }

});


// Quand le serveur reçoit une requête GET avec l'id d'une image existante, il lui renvoie.
app.get("/:id", (req, res) => {
  const image_id = req.params.id;
  const path = `images/${image_id}.webp`;
  if (!fs.existsSync(path)) {
    return res.status(404).send("Aucune image avec cet id.");
  }

  res.download(path);
});


app.listen(port, () => {
  console.log(`Serveur démarré sur http://localhost:${port}`);
});


